CHANGELOG
=========


0.1.5 (unreleased)
------------------

- Nothing changed yet.


0.1.4 (2024-09-22)
------------------

- Update requirements


0.1.3 (2021-07-14)
------------------

- Drop sql-dir option from migrate command


0.1.2 (2021-07-12)
------------------

- Do not assume pk is an uuid
- Allow direction and limit override when select
- Clean data to insert/update based on pydantic exclude_unset
- Drop useless release requirement


0.1.1 (2021-05-30)
------------------

- Fix packaging


0.1.0 (2021-05-30)
------------------

- First implementation
