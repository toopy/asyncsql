import itertools
from uuid import UUID, uuid4

import orjson
import pytest
from asyncpg.exceptions import UniqueViolationError

from asyncsql.cursor import Cursor

from ._models import Job
from .helpers import override_settings


def test_dummys__get_conditions(dummys_queries):
    index = itertools.count(start=1)

    conditions, values = dummys_queries._get_conditions(
        {"foo": "x", "bar": "y"}, index, only=(), exclude=(), operator="="
    )
    assert conditions == ["bar = $1", "foo = $2"]
    assert values == ["y", "x"]
    assert str(index) == "count(3)"

    conditions, values = dummys_queries._get_conditions(
        {"foo": "x", "bar": "y"}, index, only=("foo",), exclude=(), operator="<="
    )
    assert conditions == ["foo <= $3"]
    assert values == ["x"]
    assert str(index) == "count(4)"

    conditions, values = dummys_queries._get_conditions(
        {"foo": "x", "bar": "y"}, index, only=("foo",), exclude=("foo",), operator=">="
    )
    assert conditions == []
    assert values == []
    assert str(index) == "count(4)"

    conditions, values = dummys_queries._get_conditions(
        {"foo": "x", "bar": "y"}, index, only=(), exclude=("foo",), operator=">="
    )
    assert conditions == ["bar >= $4"]
    assert values == ["y"]
    assert str(index) == "count(5)"


def test_dummys_get_order_by(dummys_queries):
    assert dummys_queries.get_order_by() == "foo asc, bar asc"
    assert dummys_queries.get_order_by(order_fields=("foo", "what")) == "foo asc"
    assert (
        dummys_queries.get_order_by(direction="desc", order_fields=("bar",))
        == "bar desc"
    )


def test_dummys_get_where(dummy, dummys_queries):
    conditions, values, next_ = dummys_queries.get_where()
    assert not conditions
    assert not values
    assert next_ == 1

    conditions, values, next_ = dummys_queries.get_where(last_dict=dummy.model_dump())
    assert conditions == "bar >= $1 AND foo >= $2 AND id != $3"
    assert values == ["y", "x", dummy.id]
    assert next_ == 4

    conditions, values, next_ = dummys_queries.get_where(
        direction="desc",
        last_dict=dummy.model_dump(),
        or_fields=["foo"],
        query_params={"foo": "a"},
    )
    assert conditions == "bar <= $1 AND foo <= $2 AND id != $3 AND (foo = $4)"
    assert values == ["y", "x", dummy.id, "a"]
    assert next_ == 5

    conditions, values, next_ = dummys_queries.get_where(
        direction="asc",
        last_dict=dummy.model_dump(),
        or_fields=["foo", "bar"],
        query_params={"foo": "a", "bar": "b"},
    )
    assert (
        conditions == "bar >= $1 AND foo >= $2 AND id != $3 AND (bar = $4 OR foo = $5)"
    )
    assert values == ["y", "x", dummy.id, "b", "a"]
    assert next_ == 6


def test_dummys_get_insert_sql(dummy, dummys_queries):
    sql, values = dummys_queries.get_insert_sql(dummy)
    assert (
        sql
        == """
INSERT INTO dummys(id, foo, bar, data)
VALUES($1, $2, $3, $4)
RETURNING id
""".strip()
    )
    assert list(values) == [dummy.id, "x", "y", {"a": 1}]


def test_dummys_get_update_sql(dummy, dummys_queries):
    sql, values = dummys_queries.get_update_sql(dummy)
    assert (
        sql
        == """
UPDATE dummys
SET
  foo = $2,
  bar = $3,
  data = $4
WHERE
  id = $1
RETURNING id
""".strip()
    )
    assert list(values) == [dummy.id, "x", "y", {"a": 1}]


@pytest.mark.asyncio
async def test_jobs_exists(conn, job_db, jobs_queries):
    assert await jobs_queries.exists_by_id(conn, job_db.id)


@pytest.mark.asyncio
async def test_jobs_not_exists(conn, jobs_queries):
    assert not await jobs_queries.exists_by_id(conn, uuid4())


@pytest.mark.asyncio
async def test_jobs_insert(conn, job, jobs_queries):
    new_job = await jobs_queries.insert(conn, job)
    assert new_job.id


@pytest.mark.asyncio
async def test_jobs_insert_with_id(conn, job, jobs_queries):
    job.id = uuid4()
    new_job = await jobs_queries.insert(conn, job)
    assert new_job.id == job.id


@pytest.mark.asyncio
async def test_jobs_insert_conflict(conn, job_db, jobs_queries):
    with pytest.raises(UniqueViolationError):
        await jobs_queries.insert(conn, job_db)


@pytest.mark.asyncio
async def test_jobs_select_by_id(conn, job_db, jobs_queries):
    job = await jobs_queries.select_by_id(conn, job_db.id)
    assert job == job_db


@pytest.mark.asyncio
async def test_jobs_select_by_id_not_exist(conn, jobs_queries):
    assert not await jobs_queries.select_by_id(conn, str(uuid4()))


@pytest.mark.asyncio
async def test_jobs_select_json(conn, job_list_db, jobs_queries):
    # work with 3 jobs per page
    jobs_queries.per_page = 3

    rows_json, last = await jobs_queries.select_json(conn)

    job_list = orjson.loads(rows_json)
    job_last = job_list[-1]
    assert last == {k: job_last[k] for k in ["id"] + list(jobs_queries.cursor_fields)}

    assert len(job_list) == 3
    assert [UUID(j["id"]) for j in job_list] == [j.id for j in job_list_db][:3]

    job = Job(**job_list[-1])
    cursor = Cursor(
        fields=("name",),
        obj=job,
        query_params={"trigger": "interval"},
    )
    where, values, _ = jobs_queries.get_where_from_cursor(cursor)
    assert where == "name >= $1 AND id != $2 AND trigger = $3"
    assert values == [job.name, job.id, job.trigger]

    with override_settings(PER_PAGE=3):
        rows_json, last = await jobs_queries.select_json(
            conn, values=values, where=where
        )

    job_list = orjson.loads(rows_json)
    assert not last

    assert len(job_list) == 2
    assert [UUID(j["id"]) for j in job_list] == [j.id for j in job_list_db][3:]


@pytest.mark.asyncio
@pytest.mark.parametrize("per_page", (2, 5))
async def test_jobs_select_json_per_page(conn, job_list_db, jobs_queries, per_page):
    jobs_queries.per_page = per_page
    rows_json, last = await jobs_queries.select_json(conn)

    job_list = orjson.loads(rows_json)
    job_last = job_list[-1]
    if per_page == 5:
        assert not last
    else:
        assert last == {
            k: job_last[k] for k in ["id"] + list(jobs_queries.cursor_fields)
        }

    assert len(job_list) == per_page
    assert [UUID(j["id"]) for j in job_list] == [j.id for j in job_list_db][:per_page]


@pytest.mark.asyncio
async def test_jobs_select_json_where_filter(conn, job_list_db, jobs_queries):
    where, values, _ = jobs_queries.get_where(query_params={"name": "job3"})
    rows_json, last = await jobs_queries.select_json(conn, values=values, where=where)

    job_list = orjson.loads(rows_json)
    assert not last

    expected_id = [j for j in job_list_db if j.name == "job3"][0].id
    assert len(job_list) == 1
    assert UUID(job_list[0]["id"]) == expected_id
    assert job_list[0]["name"] == "job3"

    where, values, _ = jobs_queries.get_where(query_params={"trigger": "cron"})
    rows_json, last = await jobs_queries.select_json(conn, values=values, where=where)

    job_list = orjson.loads(rows_json)
    assert not job_list
    assert not last


@pytest.mark.asyncio
async def test_jobs_select_json_by_id(conn, job_db, jobs_queries):
    job_json = await jobs_queries.select_json_by_id(conn, job_db.id)
    assert Job(**orjson.loads(job_json)) == Job(**job_db.model_dump())


@pytest.mark.asyncio
async def test_jobs_select_json_by_id_not_exist(conn, jobs_queries):
    assert not await jobs_queries.select_json_by_id(conn, str(uuid4()))


@pytest.mark.asyncio
async def test_jobs_update(conn, job_db, jobs_queries):
    job_db.name = "jobx (bis)"
    job_db_new = await jobs_queries.update(conn, job_db)
    assert job_db_new.updated_at != job_db.updated_at


@pytest.mark.asyncio
async def test_jobs_update_conflict(conn, job_data, job_db, jobs_queries):
    job_db_new = await jobs_queries.insert(
        conn, Job(**{**job_data, **{"name": "jobx (bis)"}})
    )

    job_db_new.name = job_db.name

    with pytest.raises(UniqueViolationError):
        await jobs_queries.update(conn, job_db_new)


@pytest.mark.asyncio
async def test_jobs_update_not_exist(conn, job, jobs_queries):
    job.id = uuid4()
    await jobs_queries.update(conn, job)
    assert not job.updated_at
