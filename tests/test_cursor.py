from uuid import UUID

from asyncsql.cursor import Cursor

from ._models import Dummy

CURSOR = (
    "gASVdwAAAAAAAAAojANhc2OUfZQojAJpZJSMBHV1aWSUjARVVUlElJOUKYGUfZQojANpbnSUihHGIrfM"
    "xBiYnttK95Z9M4q6AIwHaXNfc2FmZZRLAHVijANmb2+UjAF4lIwDYmFylIwBeZSMBGRhdGGUfZSMAWGU"
    "SwFzdSl9lHSULg=="
)


def test_cursor(dummy):
    cursor = Cursor(obj=dummy)
    assert cursor.direction == "asc"
    assert cursor.fields == ()
    assert cursor.obj == dummy
    assert cursor.obj_fields == ["id", "foo", "bar", "data"]
    assert not cursor.or_fields
    assert not cursor.query_params


def test_cursor_from_str(dummy):
    assert Cursor.from_str(None) == Cursor()
    assert Cursor.from_str(Dummy, cursor=CURSOR) == Cursor(obj=dummy)


def test_cursor_obj_dict(dummy):
    assert Cursor(obj=dummy).obj_dict == {
        "bar": "y",
        "data": {"a": 1},
        "foo": "x",
        "id": UUID("ba8a337d-96f7-4adb-9e98-18c4ccb722c6"),
    }


def test_cursor___eq__(dummy):
    assert Cursor(obj=dummy) == Cursor(obj=dummy)


def test_cursor___repr__(dummy):
    assert str(Cursor(obj=dummy)) == CURSOR


def test_cursor_idempotent(dummy):
    cursor = Cursor(obj=dummy)
    assert Cursor.from_str(Dummy, cursor=str(cursor)) == cursor
