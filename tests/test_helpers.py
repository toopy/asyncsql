import pytest

from asyncsql.helpers import get_sql_from_file, orjson_dumps

from .helpers import override_settings


@pytest.mark.asyncio
async def test_get_sql_from_file():
    sql = await get_sql_from_file("foo")
    assert sql == "bar"


@pytest.mark.asyncio
async def test_get_sql_from_file_not_found():
    with override_settings(SQL_DIR="./nowhere"):
        with pytest.raises(FileNotFoundError):
            await get_sql_from_file("foo")


def test_orjson_dumps():
    assert orjson_dumps({"foo": "bar"}) == '{"foo":"bar"}'
