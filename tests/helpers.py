from contextlib import contextmanager

from dynaconf.base import Settings

from asyncsql.settings import settings


@contextmanager
def override_settings(**kw):
    old_settings = settings._settings._wrapped

    # drop cached and override
    for k in kw.keys():
        settings.__dict__.pop(k.lower(), None)
    settings._settings._wrapped = Settings(settings_module=None, **kw)

    yield

    # restore previous
    for k in kw.keys():
        settings.__dict__.pop(k.lower(), None)
    settings._settings._wrapped = old_settings
