-- version: 0.1.0
BEGIN;
-- required for `uuid_generate_v1` function
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
DO $$ BEGIN CREATE TYPE job_trigger_enum AS ENUM ('cron', 'date', 'interval');
EXCEPTION
WHEN duplicate_object THEN null;
END $$;
-- jobs table
CREATE TABLE IF NOT EXISTS jobs (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
  enabled BOOLEAN DEFAULT FALSE,
  name CHARACTER VARYING(128) UNIQUE NOT NULL,
  description text,
  func CHARACTER VARYING(128) NOT NULL,
  kwargs JSON,
  trigger job_trigger_enum,
  trigger_kwargs JSON,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
);
-- jobs.updated_at func
CREATE OR REPLACE FUNCTION update__jobs__updated_at__func() RETURNS trigger LANGUAGE plpgsql AS $$ BEGIN NEW.updated_at = now();
RETURN NEW;
END;
$$;
DO $$ BEGIN IF NOT EXISTS (
  SELECT 1
  FROM pg_trigger
  WHERE tgname = 'update__jobs__updated_at__trigger'
) THEN CREATE TRIGGER update__jobs__updated_at__trigger BEFORE
UPDATE ON jobs FOR EACH ROW EXECUTE PROCEDURE update__jobs__updated_at__func();
END IF;
END $$;
COMMIT;