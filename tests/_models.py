from datetime import datetime
from enum import Enum
from typing import Optional
from uuid import UUID

from asyncsql.models import Model


class Dummy(Model):
    id: Optional[UUID] = None
    foo: str
    bar: str
    data: Optional[dict] = None


class JobTriggerEnum(str, Enum):
    cron = "cron"
    date = "date"
    interval = "interval"


class Job(Model):
    id: Optional[UUID] = None
    enabled: bool = False
    name: str
    description: Optional[str] = None
    func: str
    kwargs: Optional[dict] = None
    trigger: JobTriggerEnum
    trigger_kwargs: dict
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None
