import pytest
import pytest_asyncio
from async_generator import asynccontextmanager

from asyncsql.backends import sql_backend
from asyncsql.helpers import migrate
from asyncsql.queries import Queries

from ._models import Dummy, Job


class FakeConnection:
    def __init__(self, mocker):
        self.execute_mock = mocker.Mock()
        self.fetch_mock = mocker.Mock()
        self.fetchrow_mock = mocker.Mock()

    async def execute(self, *args):
        self.execute_mock(*args)

    async def fetch(self, *args):
        self.fetch_mock(*args)
        return []

    async def fetchrow(self, *args):
        self.fetchrow_mock(*args)

    @asynccontextmanager
    async def transaction(self):
        yield


@pytest_asyncio.fixture(autouse=True)
async def conn():
    conn = await sql_backend.conn
    await migrate(conn, "jobs")
    yield conn
    await conn.execute("DELETE FROM jobs")
    await conn.close()
    sql_backend._conn = None


@pytest.fixture
def fake_conn(mocker):
    return FakeConnection(mocker)


@pytest.fixture
def dummy_id():
    return "ba8a337d-96f7-4adb-9e98-18c4ccb722c6"


@pytest.fixture
def dummy(dummy_id):
    return Dummy(id=dummy_id, foo="x", bar="y", data={"a": 1})


@pytest.fixture
def dummys_queries():
    return Queries(
        "dummys",
        cursor_fields=(
            "foo",
            "bar",
        ),
        model_cls=Dummy,
        order_fields=(
            "foo",
            "bar",
        ),
    )


@pytest.fixture
def job_data():
    return {
        "name": "jobx",
        "func": "funcx",
        "trigger": "interval",
        "trigger_kwargs": {"seconds": 2},
    }


@pytest.fixture
def job(job_data):
    return Job(**job_data)


@pytest.fixture
def jobs_queries():
    return Queries(
        "jobs",
        cursor_fields=("name",),
        model_cls=Job,
        order_fields=("created_at",),
        returning_fields=("created_at", "id", "updated_at"),
    )


@pytest_asyncio.fixture
async def job_db(conn, job, jobs_queries):
    return await jobs_queries.insert(conn, job)


@pytest_asyncio.fixture
async def job_list_db(conn, job_data, jobs_queries):
    job_list = []
    for x in range(5):
        job_list.append(
            await jobs_queries.insert(conn, Job(**{**job_data, **{"name": f"job{x}"}}))
        )
    return job_list
