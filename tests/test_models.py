from datetime import datetime
from uuid import UUID, uuid4

import orjson
import pytest
from pydantic import ValidationError

from asyncsql.models import Model

NOW = datetime.now()


class Dummy(Model):
    id: UUID
    dt_val: datetime = NOW
    dict_val: dict = {}
    str_val: str = ""


@pytest.mark.parametrize("valid_id", (str(uuid4()), uuid4()))
def test_model_valid_id(valid_id):
    assert Dummy(id=valid_id)


@pytest.mark.parametrize("invalid_id", ("", 0, None, False, "azerty"))
def test_model_invalid_id(invalid_id):
    with pytest.raises(ValidationError):
        Dummy(id=invalid_id)


def test_model_is_type_dict():
    assert not Dummy.is_type_dict("id")
    assert Dummy.is_type_dict("dict_val")


def test_model_from_record():
    _id = uuid4()
    assert Dummy.from_record({"id": _id}) == Dummy(id=_id)


def test_model_json():
    dt = datetime.now()
    id = uuid4()
    data = dict(id=str(id), dt_val=dt, dict_val={"foo": "bar"}, str_val="Foo bar...")
    assert Dummy(**data).model_dump_json() == orjson.dumps(data).decode()
